# Proyecto para agrupamiento de estudiantes #
### Requerimientos ###
Instalar manejador de paquetes PyPi
```
sudo apt-get install python-pip
```
Instalar Django
```
sudo pip install django
```

### Ejecución del proyecto ###
Ubicarse detro de la carpeta del proyecto.
Antes de correr el proyecto por primera vez, se deben crear las migraciones con:
```
python manage.py migrate
```

Para correr el proyecto en modo de pruebas, con el webserver de Django, usamos:
```
python manage.py runserver
```

Se necesita al menos un usuario administrador, se debe crear con:
```
python manage.py createsuperuser
```

### Como correr el proyecto desde Apache: ###

Para correr el servidor con Apache, debemos instalar el siguiente mod:
```
sudo apt-get install libapache2-mod-wsgi
```

No olvidar que debemos servir los archivos estáticos por separado. Se debe editar el archivo "settings.py" y agregar al final:
```    
STATIC_ROOT = os.path.join(BASE_DIR, "static/")
```

Y luego correr:
```
python manage.py collectstatic
```

Lo siguiente es configurar el mod_wsgi para que capture las peticiones, debemos editar el archivo:
```
sudo nano /etc/apache2/sites-available/000-default.conf
```

Y debe quedar algo parecido a lo siguiente:
```
<VirtualHost *:80>
    ...

    Alias /static /home/<usuario>/groupme/static

    <Directory /home/<usuario>/groupme/static>
        Allow from all
    </Directory>

    <Directory /home/<usuario>/groupme/groupme_teg>
        <Files wsgi.py>
            Order deny,allow
            Allow from all
        </Files>
    </Directory>

    WSGIDaemonProcess groupme python-path=/home/<usuario>/groupme
    WSGIProcessGroup groupme
    WSGIScriptAlias / /home/<usuario>/groupme/groupme_teg/wsgi.py
</VirtualHost>
```
Luego de editar el archivo, se debe reiniciar apache con:
```
sudo service apache2 restart
```

Si el archivo ```/etc/apache2/sites-available/000-default.conf``` no existe, podemos usar ```/etc/apache2/sites-available/default``` o alguno que se encuentre en ese directorio.

Si hay problemas para correr el servidor con Apache, revisar los siguientes tutoriales, es posible que se esté usando una versión de Apache diferente:

* [How To Serve Django Applications with Apache and mod_wsgi on Ubuntu 14.04](https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/modwsgi/)

* [How to use Django with Apache and mod_wsgi](https://www.digitalocean.com/community/tutorials/how-to-serve-django-applications-with-apache-and-mod_wsgi-on-ubuntu-14-04)
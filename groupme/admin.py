from django.contrib import admin
from .models import *

@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
	list_display = ['nombre']

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'region']
	list_filter = ['region']

@admin.register(Usuario)
class UsuarioAdmin(admin.ModelAdmin):
	list_display = ['user', 'ubicacion', 'profesor']

@admin.register(Actividad)
class ActividadAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'profesor', 'activo']

@admin.register(Opcion)
class OpcionAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'valor', 'categoria']

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
	list_display = ['nombre', 'balanceada']

@admin.register(Respuesta)
class RespuestaAdmin(admin.ModelAdmin):
	list_display = ['estudiante', 'actividad', 'categoria', 'opcion']

@admin.register(Peso)
class PesoAdmin(admin.ModelAdmin):
	list_display = ['valor', 'actividad', 'categoria']
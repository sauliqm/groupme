#-*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

class Usuario(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	ubicacion = models.ForeignKey('Ciudad', related_name='usuarios', null=True)
	profesor = models.BooleanField(default=False)
	def __str__(self):
		return (self.user.first_name+' '+self.user.last_name).encode('utf-8')

class Region(models.Model):
	nombre = models.CharField(max_length=32, unique=True)
	def __str__(self):
		return self.nombre.encode('utf-8')

class Ciudad(models.Model):
	nombre = models.CharField(max_length=32)
	region = models.ForeignKey('Region', related_name='ciudades', on_delete=models.CASCADE)
	def __str__(self):
		return self.nombre.encode('utf-8')

class Actividad(models.Model):
	nombre = models.CharField(max_length=64)
	profesor = models.ForeignKey('Usuario', related_name='actividades')
	num_grupos = models.IntegerField()
	categorias = models.ManyToManyField('Categoria')
	estudiantes = models.ManyToManyField('Usuario')
	activo = models.BooleanField(default=True)
	heterogenea = models.BooleanField(default=False)
	def __str__(self):
		return self.nombre.encode('utf-8')

class Categoria(models.Model):
	nombre = models.CharField(max_length=32)
	balanceada = models.BooleanField()
	def __str__(self):
		return self.nombre.encode('utf-8')

class Peso(models.Model):
	valor = models.IntegerField()
	actividad = models.ForeignKey('Actividad', related_name='pesos')
	categoria = models.ForeignKey('Categoria', related_name='pesos')
	def __str__(self):
		return ('Peso de %s en %s' %(self.actividad,self.categoria)).encode('utf-8')
	def __int__(self):
		return self.valor

class Opcion(models.Model):
	nombre = models.CharField(max_length=32)
	valor = models.IntegerField()
	categoria = models.ForeignKey('Categoria', related_name='opciones', on_delete=models.CASCADE)
	def __str__(self):
		return self.nombre.encode('utf-8')

class Grupo(models.Model):
	actividad = models.ForeignKey('Actividad', related_name='grupos', on_delete=models.CASCADE)
	estudiantes = models.ManyToManyField('Usuario')
	def __str__(self):
		return ('Grupo de '+ str(self.actividad)+ ', ID: ' +str(self.pk)).encode('utf-8')

class Respuesta(models.Model):
	estudiante = models.ForeignKey('Usuario', related_name='respuestas', on_delete=models.CASCADE)
	actividad = models.ForeignKey('Actividad', related_name='respuestas', on_delete=models.CASCADE)
	categoria = models.ForeignKey('Categoria', related_name='respuestas', on_delete=models.CASCADE)
	opcion = models.ForeignKey('Opcion', related_name='respuestas', on_delete=models.CASCADE)
	def __int__(self):
		return self.opcion.valor
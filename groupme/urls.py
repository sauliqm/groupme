#-*- coding:utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
	# Vista de inicio, para logearse o registrarse
    url(r'^$', views.index, name='index'),
    # Controlador de registro de usuario
    url(r'^register$', views.register_controller, name='register'),
    # Controlador de inicio de sesión de usuario
    url(r'^login$', views.login_controller, name='login'),
    # Controlador de cerrar sesión de usuario
    url(r'^logout$', views.logout_controller, name='logout'),
    # Vista de perfil de usuario conectado
    url(r'^perfil$', views.profile, name='profile'),
    # Vista para editar datos básicos de usuario
    url(r'^perfil/editar$', views.editProfile, name='editProfile'),
    # Vista para cambiar contraseña
    url(r'^perfil/contrasena$', views.contrasena, name='contrasena'),
    # Vista de inicio de usuario conectado
    url(r'^inicio$', views.home, name='home'),
    # Vista de administración
    url(r'^administracion$', views.adminPanel, name='adminPanel'),
    # Vista de admin para control de roles
    url(r'^administracion/roles$', views.roles, name='roles'),
    # Vista de admin para cambiar rol del usuario
    url(r'^administracion/roles/cambiar/(?P<usuario>.*)$', views.cambiarRol, name='cambiarRol'),
    # Vista de admin para control de regiones
    url(r'^administracion/regiones$', views.regiones, name='regiones'),
    # Vista de admin para agregar nuevas regiones
    url(r'^administracion/regiones/agregar$', views.addRegion, name='addRegion'),
    # Vista para agregar ciudades a una región
    url(r'^administracion/regiones/(?P<region>.*)/ciudades/agregar$', views.addCiudad, name='addCiudad'),
    # Vista de admin para control de categorias
    url(r'^administracion/categorias$', views.categorias, name='categorias'),
    # Vista para agregar una nueva categoría
    url(r'^administracion/categorias/agregar$', views.addCategoria, name='addCategoria'),
    # Vista para agregar una opción a una categoría
    url(r'^administracion/categorias/(?P<categoria>.*)/opciones/agregar$', views.addOpcion, name='addOpcion'),
    # Vista para las actividades
    url(r'^actividades$', views.actividades, name='actividades'),
    # Vista para crear nueva actividad
    url(r'^actividades/agregar$', views.addActividad, name='addActividad'),
    # Vista para inscribirse en una actividad (alumno)
    url(r'^actividades/(?P<actividad_id>[0-9]+)/inscribirse$', views.inscribirse, name='inscribirse'),
    # Vista para ver detalles de actividad (profesor)
    url(r'^actividades/(?P<actividad_id>[0-9]+)/detalles$', views.detalles, name='detalles'),
    # Controlador para salirse de una actividad
    url(r'^actividades/(?P<actividad_id>[0-9]+)/salir$', views.desmatricular, name='desmatricular'),
    # Controlador para agrupar estudiantes de una actividad
    url(r'^actividades/(?P<actividad_id>[0-9]+)/agrupar$', views.agrupar, name='agrupar'),
    # Controlador para agrupar estudiantes de una actividad
    url(r'^actividades/(?P<actividad_id>[0-9]+)/cerrar$', views.cerrar, name='cerrar'),
]
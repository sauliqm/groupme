# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-27 11:31
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('groupme', '0003_auto_20170327_0724'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usuario',
            old_name='usuario',
            new_name='user',
        ),
    ]

# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-29 12:34
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('groupme', '0010_actividad_activo'),
    ]

    operations = [
        migrations.AddField(
            model_name='actividad',
            name='estudiantes',
            field=models.ManyToManyField(to='groupme.Usuario'),
        ),
    ]

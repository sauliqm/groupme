#-*- coding:utf-8 -*-
from django import template
register = template.Library()
from ..models import *
from django.contrib.auth.models import User

@register.filter(name='valor')
def valor(val):
	return len(val.opciones.all()) + 1

@register.filter(name='respuestas')
def respuestas(estudiante, actividad):
	return Respuesta.objects.filter(estudiante=estudiante, actividad=actividad)

@register.filter(name='estudiantes_agrupados')
def estudiantes_agrupados(actividad):
	total = 0
	for grupo in actividad.grupos.all():
		total += len(grupo.estudiantes.all())
	return total

@register.filter(name='user_valido')
def user_valido(user):
	return False if not user.usuario.ubicacion or not user.first_name or not user.last_name else True

@register.filter(name='en_grupo')
def en_grupo(actividad,estudiante):
	for grupo in actividad.grupos.all():
		if estudiante in grupo.estudiantes.all():
			return True
	return False

@register.filter(name='grupos_activos')
def grupos_activos(estudiante):
	return estudiante.grupo_set.filter(actividad__activo=True)

@register.filter(name='id_grupo')
def id_grupo(actividad,estudiante):
	return estudiante.grupo_set.get(actividad=actividad).pk

@register.filter(name='es_ubicacion')
def es_ubicacion(opcion,usuario):
	if str(opcion) == str(usuario.ubicacion):
		return True
	else:
		return False

@register.filter(name='peso')
def peso(categoria, actividad):
	return [star for star in range(0, int(Peso.objects.get(categoria=categoria, actividad=actividad)))]
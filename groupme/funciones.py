#-*- coding:utf-8 -*-
from .models import *

def diferencia(lista1,lista2):
	total = 0
	for i in range(0,len(lista1)):
		d = lista1[i] - lista2[i]
		if d < 0: d *= -1
		total += d
	return total

def iniciar_clusters(matriz, k):
	# Creamos los k clusters:
	keys = matriz.keys()
	clusters = {}
	for c in range(0,k):
		clusters[c] = []
	# Luego los llenamos con las keys de la matriz
	i = 0
	for key in keys:
		clusters[i].append(key)
		i += 1
		if i > k-1: i = 0
	return clusters

# matriz: matriz con diccionario de valores de las respuestas
# clusters: clusters inicializados
# n: cantidad de categorias que tiene cada respuesta
def valor_clusters(matriz,clusters,n):
	valores = {}
	for c in clusters:
		valores[c] = []
		cluster = clusters[c]
		for cat in range(0,n):
			min_valor = 9999
			max_valor = 0
			for estudiante in cluster:
				if matriz[estudiante][cat] > max_valor:
					max_valor = matriz[estudiante][cat]
				if matriz[estudiante][cat] < min_valor:
					min_valor = matriz[estudiante][cat]
			avg = float(min_valor + max_valor) / 2
			valores[c].append(avg)
	return valores

"""
valores: Valores que fueron previamente calculados para cada cluster
matriz: matriz con diccionario de valores de las respuestas
"""
def diff_clusters(valores,matriz):
	diferencias = {}
	for cluster in valores:
		diferencias[cluster] = {}
		for estudiante in matriz:
			diferencias[cluster][estudiante] = diferencia(matriz[estudiante], valores[cluster])
	return diferencias

# diff: Diferencia previamente calculada de los clusters respecto a la matriz,
# esto generara los nuevos clusters para la siguiente iteracion				
def calc_clusters(diff, num_estudiantes):
	k = len(diff) # Cantidad de clusters nuevos a generar
	tolerancia = num_estudiantes % k
	nuevos = {}
	# Iniciamos los nuevos clusters
	for d in diff:
		nuevos[d] = []
	asignados = [] # listado de estudiantes que ya han sido asignados
	for recorrido in range(0,num_estudiantes):
		minimo = {
			'cluster': 999,
			'estudiante': 'alguien',
			'valor': 99999
		}
		for cluster in diff: # Recorremos los clusters
			for estudiante in diff[cluster]:
				if diff[cluster][estudiante] < minimo['valor'] and estudiante not in asignados and len(nuevos[cluster]) < num_estudiantes // k:
					minimo['valor'] = diff[cluster][estudiante]
					minimo['estudiante'] = estudiante
					minimo['cluster'] = cluster
					encontrado = False
				elif diff[cluster][estudiante] < minimo['valor'] and estudiante not in asignados and tolerancia > 0 and len(nuevos[cluster]) < (num_estudiantes // k) + 1:
					minimo['valor'] = diff[cluster][estudiante]
					minimo['estudiante'] = estudiante
					minimo['cluster'] = cluster
					encontrado = True
		asignados.append(minimo['estudiante'])
		nuevos[minimo['cluster']].append(minimo['estudiante'])
		if encontrado: tolerancia -= 1
	return nuevos

def crear_matriz(actividad):
	matriz = {}
	for estudiante in actividad.estudiantes.all():
		matriz[estudiante.pk] = []
		for categoria in actividad.categorias.all():
			valor = int(estudiante.respuestas.get(actividad=actividad, categoria=categoria))
			peso = int(Peso.objects.get(actividad=actividad, categoria=categoria))
			matriz[estudiante.pk].append(valor*peso)
	return matriz

def agrupar_estudiantes(matriz, num_est, grupos, num_cat):
	clusters = iniciar_clusters(matriz,grupos)
	clusters_viejos = []
	intentos = 100
	
	while diferencia_clusters(clusters_viejos, clusters) and intentos > 0:
		valores = valor_clusters(matriz,clusters,num_cat)
		diferencias = diff_clusters(valores,matriz)
		clusters_viejos.append(clusters)
		clusters = calc_clusters(diferencias,num_est)
		intentos -= 1
	return clusters

def diferencia_clusters(clusters_viejos,cluster_nuevo):
	counter = 0
	lista_2 = [set(cluster_nuevo[cluster]) for cluster in cluster_nuevo]
	if clusters_viejos == []:
		return True
	for cluster_viejo in clusters_viejos:
		lista_1 = [set(cluster_viejo[cluster]) for cluster in cluster_viejo]
		for cluster in lista_2:
			if cluster not in lista_1:
				counter += 1
				pass
	return False if counter == len(clusters_viejos) else True

def clusters_heterogeneos(clusters):
	k = len(clusters)
	nuevos = {}
	for cluster in clusters:
		nuevos[cluster] = []
	sig = 0
	for cluster in clusters:
		for estudiante in clusters[cluster]:
			nuevos[sig].append(estudiante)
			sig = sig + 1 if sig < k -1 else 0
	return nuevos
			 

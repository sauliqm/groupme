#-*- coding:utf-8 -*-
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.admin.views.decorators import staff_member_required
from django.views.decorators.http import *
from django.contrib import messages
from .models import Region, Ciudad, Usuario, Categoria, Peso, Opcion, Actividad, Respuesta, Grupo
from funciones import *

def usuario_valido(user):
	try:
		ub = user.usuario.ubicacion
		pn = user.first_name
		ap = user.last_name
		if not ub or not pn or not ap:
			return False
		else:
			return True
	except:
		return False

def index(request):
	if request.user.is_authenticated:
		return redirect('home')
	return render(request,'index.html')

@require_POST
def login_controller(request):
	username = request.POST['username']
	password = request.POST['password']
	usuario = authenticate(username=username, password=password)
	if usuario is not None:
		login(request,usuario)
		return redirect('home')
	else:
		messages.info(request,'Combinación de usuario y contraseña inválido.')
		return redirect('index')
	
def logout_controller(request):
	logout(request)
	return redirect('index')

@require_POST
def register_controller(request):
	username = request.POST['username']
	password = request.POST['password']
	password2 = request.POST['password2']
	correo = request.POST['email']

	if password != password2:
		messages.success(request, 'Las contraseñas no coinciden.')
		return redirect('index')

	if User.objects.filter(email=correo):
		messages.error(request, 'Ya existe un usuario con ese correo.')
		return redirect('index')
	
	try:
		usuario = User.objects.create_user(username, correo, password)
		usuario_obj = Usuario(user=usuario)
		usuario_obj.save()
		login(request,usuario)
		return redirect('profile')
	except Exception,e:
		messages.warning(request,'Usuario inválido o ya existe.')
		return redirect('index')

@staff_member_required
@login_required(login_url='/')
def adminPanel(request):
	return render(request,'admin/home.html')

@login_required(login_url='/')
@user_passes_test(usuario_valido, login_url='/perfil', redirect_field_name=None)
def home(request):
	# Validaciones
	regiones = True if len(Region.objects.all()) == 0 else False
	categorias = True if len(Categoria.objects.exclude(opciones=None)) == 0 else False
	profesor = True if len(Usuario.objects.filter(profesor=True)) == 0 else False
	context = {
		'regiones': regiones,
		'categorias': categorias,
		'profesor': profesor
	}
	return render(request,'home.html', context)

@login_required(login_url='/')
@staff_member_required
def roles(request):
	context = {
		'usuarios': User.objects.all()
	}
	return render(request, 'admin/roles.html',context)

@login_required(login_url='/')
@staff_member_required
def cambiarRol(request,usuario):
	usuario = Usuario.objects.get(user__username=usuario)
	usuario.profesor = True if not usuario.profesor else False
	usuario.save()
	return redirect('roles')

@login_required(login_url='/')
@staff_member_required
def regiones(request):
	if 'nombre' in request.POST:
		nombre = request.POST['nombre'].lower().strip()
		try:
			region = Region(nombre=nombre)
			region.save()
		except:
			messages.error(request,'Nombre de región ya existe.')
			return redirect('addRegion')
	elif 'ciudad' in request.POST:
		ciudad = request.POST['ciudad'].lower().strip()
		region = request.POST['region'].lower().strip()
		region = Region.objects.get(nombre=region)
		try:
			region.ciudades.get(nombre=ciudad)
			messages.error(request,'Esta región ya tiene una ciudad con ese nombre.')
			return redirect('addCiudad',str(region))
		except:
			region.ciudades.create(nombre=ciudad)
			region.save()
			try:
				categoria = Categoria.objects.get(nombre='ubicacion')
			except:
				categoria = Categoria(nombre='ubicacion', balanceada=False)
				categoria.save()	
			cant_ciu = len(categoria.opciones.all()) 
			categoria.opciones.create(nombre=ciudad, valor=cant_ciu+1)
			categoria.save()

	context = {
		'regiones': Region.objects.all()
	}
	return render(request, 'admin/regiones.html', context)

@login_required(login_url='/')
@staff_member_required
def addRegion(request):
	return render(request, 'admin/addregion.html')

@login_required(login_url='/')
@staff_member_required
def addCiudad(request, region):
	try:
		region = Region.objects.get(nombre=region)
	except:
		return redirect('regiones')
	context = {
		'region': region
	}
	return render(request, 'admin/addciudad.html', context)

@login_required(login_url='/')
def profile(request):
	try:
		usuario = Usuario.objects.get(user=request.user)
	except:
		usuario = Usuario(user=request.user)
		usuario.save()

	if request.method == 'POST':
		user = request.user
		user.first_name = request.POST['nombre'].lower().strip()
		user.last_name = request.POST['apellido'].lower().strip()
		user.email = request.POST['email'].lower().strip()
		user.save()
		try:
			region, ciudad = request.POST['region'].split(':')
			usuario.ubicacion = Ciudad.objects.get(nombre=ciudad, region__nombre=region)
			usuario.save()
		except:
			messages.error(request, 'Solo puedes escoger una ubicación de las ya existentes.')
			return redirect('editProfile')
	return render(request,'profile.html')

@login_required(login_url='/')
def editProfile(request):
	regiones = Region.objects.all()
	if len(regiones) == 0:
		return redirect('home')
	context = {
		'regiones': regiones
	}
	return render(request,'editprofile.html',context)

@login_required(login_url='/')
def contrasena(request):
	if 'actual' in request.POST:
		actual = request.POST['actual']
		nueva1 = request.POST['nueva1']
		nueva2 = request.POST['nueva2']
		usuario = authenticate(username=request.user, password=actual)
		if usuario is not None:
			if nueva1 == nueva2:
				usuario = User.objects.get(username=request.user.username)
				usuario.set_password(nueva1)
				usuario.save()
				return redirect('logout')
			else:
				messages.error(request, 'Las contraseñas nuevas que indicaste no concuerdan, debes indicar la misma contraseña para ambos campos.')
		else:
			messages.error(request, 'Contraseña actual no válida.')
	return render(request,'password.html')

@login_required(login_url='/')
@staff_member_required
def categorias(request):
	if 'nombre' in request.POST:
		nombre = request.POST['nombre'].lower().strip()
		balanceada = True if request.POST.get('balanceada',False) else False
		try:
			Categoria.objects.get(nombre=nombre)
			messages.error(request,'Ya existe una categoría con este nombre.')
			return redirect('addCategoria')
		except:
			categoria = Categoria(nombre=nombre, balanceada=balanceada)
			categoria.save()
	elif 'opcion' in request.POST:
		opcion = request.POST['opcion'].lower().strip()
		categoria = Categoria.objects.get(nombre=request.POST['categoria'])
		valor = request.POST['valor']
		try:
			Opcion.objects.get(nombre=opcion, categoria=categoria)
			messages.error(request,'Esta categoría ya tiene una opción con ese nombre.')
			return redirect('addOpcion',str(categoria))
		except:
			opcion = Opcion(nombre=opcion, valor=valor, categoria=categoria)
			opcion.save()

	context = {
		'categorias': Categoria.objects.all()
	}
	return render(request,'admin/categorias.html', context)

@login_required(login_url='/')
@staff_member_required
def addCategoria(request):
	return render(request, 'admin/addcategoria.html')

@login_required(login_url='/')
@staff_member_required
def addOpcion(request, categoria):
	if categoria == 'ubicacion': return redirect('categorias')
	try:
		categoria = Categoria.objects.get(nombre=categoria)
	except:
		return redirect('categorias')
	context = {
		'categoria': categoria
	}
	return render(request, 'admin/addopcion.html', context)

@login_required(login_url='/')
@user_passes_test(usuario_valido, login_url='/perfil', redirect_field_name=None)
def actividades(request):
	if 'nombre' in request.POST and request.user.usuario.profesor:
		nombre = request.POST['nombre'].lower().strip()
		numero = request.POST['numero']
		categorias = request.POST.getlist('categorias')
		balanceada = True if request.POST.getlist('tipo')[0].decode('utf-8') == 'hete' else False
		try:
			Actividad.objects.get(nombre=nombre, profesor=request.user.usuario, activo=True)
			messages.error(request,'Ya existe una actividad con el mismo nombre.')
		except:
			actividad = Actividad(nombre=nombre, profesor=request.user.usuario, num_grupos=int(numero), heterogenea=balanceada)
			actividad.save()
			sin_categorias = True
			for categoria in categorias:
				cat_obj = Categoria.objects.get(id=int(categoria))
				if cat_obj.balanceada == balanceada:
					sin_categorias = False
					actividad.categorias.add(cat_obj)
					valor = request.POST['peso'+str(cat_obj.pk)]
					peso = Peso(valor=valor, actividad=actividad, categoria=cat_obj)
					peso.save()
			if sin_categorias:
				messages.error(request,'Debes elegir al menos una categoría.')
				actividad.delete()
				return redirect('addActividad')
			actividad.save()

	elif 'nombre' in request.POST and not request.user.usuario.profesor:
		messages.error(request,'Su usuario no es profesor, no puede crear actividades.')
	elif 'actividad' in request.POST:
		actividad = Actividad.objects.get(pk=request.POST['actividad'])
		estudiante = request.user.usuario
		for categoria in actividad.categorias.all():
			opcion = Opcion.objects.get(pk=request.POST[str(categoria.pk)])
			respuesta = Respuesta(estudiante=estudiante, actividad=actividad, categoria=categoria, opcion=opcion)
			respuesta.save()
		actividad.estudiantes.add(estudiante)
		actividad.save()

	if request.user.usuario.profesor:
		proyectos = Actividad.objects.filter(profesor=request.user.usuario, activo=True)
	else:
		proyectos = Actividad.objects.filter(activo=True)
	context = {
		'actividades': proyectos
	}
	return render(request, 'actividades/index.html', context)

@login_required(login_url='/')
@user_passes_test(usuario_valido, login_url='/perfil', redirect_field_name=None)
def addActividad(request):
	if not request.user.usuario.profesor:
		messages.error(request,'Su usuario no es profesor, no puede crear actividades.')
		return redirect('actividades')
	categorias = Categoria.objects.exclude(opciones=None)
	if len(categorias) == 0:
		return redirect('home')
	context = {
		'categorias': categorias
	}
	return render(request, 'actividades/addactividad.html', context)

@login_required(login_url='/')
@user_passes_test(usuario_valido, login_url='/perfil', redirect_field_name=None)
def inscribirse(request, actividad_id):
	actividad = get_object_or_404(Actividad, pk=actividad_id)
	if request.user.is_staff:
		messages.error(request, 'Los administradores del sistema no se pueden inscribir en las actividades.')
		return redirect('actividades')
	if request.user.usuario in actividad.estudiantes.all():
		messages.error(request, 'Ya estás inscrito en esta actividad.')
		return redirect('actividades')
	context = {
		'actividad': actividad
	}
	return render(request, 'actividades/inscribirse.html', context)

@login_required(login_url='/')
@user_passes_test(usuario_valido, login_url='/perfil', redirect_field_name=None)
def detalles(request, actividad_id):
	actividad = get_object_or_404(Actividad, pk=actividad_id)
	if actividad.profesor != request.user.usuario:
		messages.error(request,'Solo el profesor %s puede ver los detalles de la actividad %s.' %(str(actividad.profesor).title(), str(actividad).title()))
		return redirect('actividades')
	context = {
		'actividad': actividad
	}
	return render(request, 'actividades/detalles.html', context)

@login_required(login_url='/')
@user_passes_test(usuario_valido, login_url='/perfil', redirect_field_name=None)
def desmatricular(request, actividad_id):
	actividad = get_object_or_404(Actividad, pk=actividad_id)
	if request.user.usuario not in actividad.estudiantes.all():
		messages.error(request, 'Ya te habías salido de esa actividad.')
		return redirect('actividades')
	respuestas = Respuesta.objects.filter(estudiante=request.user.usuario, actividad=actividad)
	for respuesta in respuestas: respuesta.delete()
	actividad.estudiantes.remove(request.user.usuario)
	actividad.save()
	return redirect('actividades')

@login_required(login_url='/')
@user_passes_test(usuario_valido, login_url='/perfil', redirect_field_name=None)
def agrupar(request, actividad_id):
	actividad = get_object_or_404(Actividad, pk=actividad_id, profesor=request.user.usuario)
	actividad.grupos.all().delete() # Se borran los grupos de esa actividad

	matriz = crear_matriz(actividad)
	num_est = len(actividad.estudiantes.all())
	grupos = actividad.num_grupos
	num_cat = len(actividad.categorias.all())
	clusters = agrupar_estudiantes(matriz, num_est, grupos, num_cat)
	if actividad.heterogenea:
		clusters = clusters_heterogeneos(clusters)
	for cluster in clusters:
		grupo = Grupo(actividad=actividad)
		grupo.save()
		for estudiante in clusters[cluster]:
			grupo.estudiantes.add(Usuario.objects.get(pk=estudiante))
		grupo.save()
	return redirect('detalles', actividad_id)

@login_required(login_url='/')
@user_passes_test(usuario_valido, login_url='/perfil', redirect_field_name=None)
def cerrar(request, actividad_id):
	actividad = get_object_or_404(Actividad, pk=actividad_id, profesor=request.user.usuario)
	actividad.activo = False
	actividad.save()
	return redirect('actividades')